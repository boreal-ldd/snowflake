# Nom Long
FR : Snowflake : Programmation Dynamique Générique et Symbolique
EN : Snowflake : A Generic Symbolic Dynamic Programming framework

# Accroche

FR: Snowflake fournit une interface entre un solveur pour WAP (Weighted Adjacency Propagation problem), une implémentation de CoSTreD (Reduced Block Triangular Form) à base de foncteur, ainsi qu'une boîte à outils minimaliste pour s'interfacer avec MLBDD (une bibliothèque de manipualtion de BDD).
EN: Snowflake interfaces a WAP-solver (Weighted Adjacency Propagation problem), a functor-based implementation of CoSTreD (Reduced Block Triangular Form), along with a minimalist MLBDD (Arlen Cox's BDD package) toolbox.

# Description fonctionelle

FR: Snowflake fournit une interface entre un solveur pour WAP (Weighted Adjacency Propagation problem), une implémentation de CoSTreD (Reduced Block Triangular Form) à base de foncteur, ainsi qu'une boîte à outils minimaliste pour s'interfacer avec MLBDD (une bibliothèque de manipualtion de BDD).
EN: Snowflake interfaces a WAP-solver (Weighted Adjacency Propagation problem), a functor-based implementation of CoSTreD (Reduced Block Triangular Form), along with a minimalist MLBDD (Arlen Cox's BDD package) toolbox.

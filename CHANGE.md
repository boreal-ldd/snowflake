# RELEASE v0.02.03 Renamed RBTF into CoSTreD

## 2022/06/30 : renamed RBTF into CoSTreD

## 2022/06/19 : added basic constraint system export [MR !13]
- **NEW** file dumping of the constraint system
- **NEW** graph output model (in graphviz export)
- **NOTE** replaced Tree.gnext by AB.ab

## 2022/06/02 : add small graphviz interface
- **NEW** added `export_graphviz` option `compute` methods in `MlbddExample`
- **NEW** added CHANGE file
- **FIX** added back `lightspeed_lopt` method in lightspeed's mli interface
- **NEW** Used GuaCaml's conversion facility from GraphKD to `GGLA_HFT` (ex GraphHFT)

## 2022/06/02 : added small graphviz interface
- **NEW** `~export_graphviz:string option` option to `compute` methods in `MlbddExample`

## 2022/06/02 : added `sorted` test on input to `MlbddUtils.subst`

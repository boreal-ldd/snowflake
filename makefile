# LGPL-3.0 Linking Exception
#
# Copyright (c) 2020 Joan Thibault (joan.thibault@irisa.fr)
#
# Snowflake : A Generic Symbolic Dynamic Programming framework interfacing WAP, CoSTreD and MLBDD.

NPROC=$(shell nproc || echo 1)

OB=ocamlbuild -r -j $(NPROC) -use-ocamlfind

.PHONY: all install uninstall ounits clean

all :
	$(OB) \
		Snowflake.d.cma \
		Snowflake.cma \
		Snowflake.cmi \
		Snowflake.cmx \
		Snowflake.cmxa \
		Snowflake.cmxs

install : all
	ocamlfind remove  Snowflake
	ocamlfind install Snowflake META _build/Snowflake.*

uninstall : all
	ocamlfind remove  Snowflake

ounits :
	$(OB) -I src \
		ounits/ounit_wap.native \
		ounits/ounit_wap.d.byte

clean:
	ocamlbuild -clean


# Snowflake

Generic Symbolic Dynamic Programming,
Contains a WAP-solver (Weighted Adjacency Propagation problem), a functor-based implementation of CoSTreD (Reduced Block Triangular Form), along with a minimalist MLBDD (Arlen Cox's BDD package) toolbox.

One can use it to solve (possibly Quantified) CNF, (possibly parametrized) integer programming, asynchronous automata composition (although we did not try that one yet).
And by solve, we compute a representation of the full set of solution not just a single one[^1], which is sometime useful.

[^1]: with the right set of parameters, one can also compute a single solution if one does not need more.

Snowflake currently uses binary decision diagrams to reason symbolically, but other symbolic formalism could be used in their place.

## Scientific Contribution

Complex systems (either physical or logical) are usually structured and sparse, that is, they are build from individual components linked together, and any component is only linked to a rather small number of other components with respects to the size of the global system.

CoSTreD exploits this structure, by over-approximating the relations between components as a tree (called decomposition tree in the graph literature) each node of this tree being a set of components of the initial systems.
Then, starting from leaves, each sub-system is solved and the solutions are projected as a new constraints on their parent node, this process is iterated until all sub-systems are solved.
This step allows to condensate all constraints into a single sub-system and check their satisfiability.
We call this step the **Forward Reduction Process** (FRP).

Finally, we can propagate all the constraints back into their initial sub-system by performing those same projection in the reverse direction.
That is, each sub-system update its set of solution given the information from its parent then send the information to its children sub-systems (possibly none, if its a leaf).
We call this step the **Backward Propagation Process** (BPP).

## Citations

```bibtex
@software{Snowflake,
	author = {{Thibault, Joan}},
	title = {Snowflake},
	url = {https://gitlab.com/boreal-ldd/snowflake},
	version = {0.02.02},
	date = {2021-10-15},
}
```

[Constraint System Decomposition, Hal Inria, J. Thibault, 2022](https://hal.archives-ouvertes.fr/hal-03740562/)

```bibtex
@techreport{thibault:hal-03740562,
  TITLE = {{Constraint System Decomposition}},
  AUTHOR = {Thibault, Joan},
  URL = {https://hal.inria.fr/hal-03740562},
  TYPE = {Research Report},
  NUMBER = {RR-9478},
  PAGES = {1-68},
  INSTITUTION = {{Inria Rennes}},
  YEAR = {2022},
  MONTH = Jul,
  DOI = {10.13140/RG.2.2.13004.49285},
  KEYWORDS = {tree decomposition ; treewidth ; message passing ; belief propagation ; factor graph ; chordal networks ; constraint networks ; symbolic representation ; symbolic computation ; Snowflake ; d{\'e}composition aborescente ; largeur aborescente ; r{\'e}seaux de contraintes ; repr{\'e}sentation symbolique ; calcul symbolique ; calcul formel ; Snowflake},
  PDF = {https://hal.inria.fr/hal-03740562v2/file/inria-template.pdf},
  HAL_ID = {hal-03740562},
  HAL_VERSION = {v2},
}
```

## Install

### Via `opam`

Using the package manager [opam](https://opam.ocaml.org/),

```shell
	opam install snowflake
```


To install the current development version:

```shell
	opam pin add https://gitlab.com/boreal-ldd/snowflake.git#master
```

### From Sources

Installation command for Snowflake

```
cd snowflake
make install
```

#### Compilation Error

By default, should any compilation error occur, first try cleaning the compilation unit and start again:
```
make clean
make install
```

##### Inconsistent Library
if ocamlbuild returns an error containing the following message:
```
make inconsistent assumptions over interface GuaCaml
```
or just
```
make inconsistent assumptions
```

cf. clean the compilation unit and restart
if the same problem persists contact the development team


##### Another Error ? 
First, make sure that your current version is up to date, clean the compilation unit and restart:
If your repository is up to date and you already tried cleaning the compilation unit, then contact the development team.

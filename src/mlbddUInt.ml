(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * Snowflake : A Generic Symbolic Dynamic Programming framework interfacing WAP, CoSTreD and MLBDD.
 *
 * MlbddUInt : Unsigned Integers (a.k.a., natural numbers) encoded as a list of BDD using little-endian encoding
 * remark : this file was mostly imported from DAGaml's oops/vUInt
 *)

open GuaCaml
open MlbddUtils

let prefix = "Snowflake.MlbddUint"

let fulladd2 a b =
  let carry = a |! b in
  let somme = a ^! b in
  (somme, carry)

let fulladd3 a b c =
  let carry = (a |! (b &! c)) &! (b |! c) in
  let somme = a ^! b ^! c in
  (somme, carry)

type uint = f array

let uint_get t ?default x intX = if x < (Array.length intX)
  then intX.(x)
  else (match default with
    | Some default -> default
    | None -> (cst t false)
  )

let map f intX = Array.map f intX

let uint_of_bool (f:f) : uint = [|f|]

let uint_of_int t x =
  assert(x>=0);
  let bin = Tools.bin_of_int x in
  Array.map (fun b -> cst t b) bin

let uint_one t = [|cst t true|]

let add t ?carry (intX:uint) (intY:uint) : uint =
  let lenX = Array.length intX
  and lenY = Array.length intY in
  let carry = ref
   (match carry with
    | None -> (cst t false)
    | Some f -> f
   )
  in
  let len = max lenX lenY in
  Array.init (len+1)
  (fun i ->
    if i = len
    then (!carry)
    else
    (
      let c = !carry in
      let x = uint_get t i intX
      and y = uint_get t i intY in
      let xy, c = fulladd3 x y c in
      carry := c;
      xy
    )
  )

let ( +/ ) t x y = add t x y

let array_add t fa = Tools.tree_of_array (add t) fa
let  list_add t fl = Tools.tree_of_list  (add t) fl

let ( +?/ ) t uintX intY = (+/) t uintX (uint_of_int t intY)
let ( ?+/ ) t intX uintY = (+/) t (uint_of_int t intX) uintY

(* decreasing shift *)
let shift_right t shift intX =
  let len = Array.length intX in
  if shift < len
  then (Array.sub intX shift (len-shift))
  else [||]

let ( >>/ ) t intX shift = shift_right t shift intX

(* increasing shift *)
let shift_left t shift intX =
  let len = Array.length intX
  and zero = cst t false in
  Array.init (len+shift) (fun i ->
    let i' = i - shift in
    if i' >= 0 then intX.(i') else zero)

let ( <</ ) t intX shift = shift_left t shift intX

let bitwise_binop (binop:f->f->f) (t:t) ?defaultX ?defaultY intX intY =
  let lenX = Array.length intX
  and lenY = Array.length intY in
  let defaultX = match defaultX with Some edge -> edge | None -> (cst t false)
  and defaultY = match defaultY with Some edge -> edge | None -> (cst t false) in
  Array.init (max lenX lenY) (fun i ->
    let x = uint_get t ~default:defaultX i intX
    and y = uint_get t ~default:defaultY i intY in
    binop x y)

let ( |&/ ) x y = bitwise_binop ( &! ) x y
let ( |^/ ) x y = bitwise_binop ( ^! ) x y
let ( ||/ ) x y = bitwise_binop ( |! ) x y
let ( |=/ ) x y = bitwise_binop ( =! ) x y

let bitwise_choice t bX int0 int1 =
  bitwise_binop ((fun t -> fite t bX)) t int0 int1

let zero_choice t bX intY =
  bitwise_choice t bX [||] intY

let shift_right' t shiftX intY =
  let result = ref intY in
  for i = 0 to (Array.length shiftX - 1)
  do
    result := bitwise_choice t shiftX.(i) !result (shift_right t (1 lsl i) !result);
  done;
  !result

let ( >>// ) t intY shiftX = shift_right' t shiftX intY

let shift_left' t shiftX intY =
  let result = ref intY in
  for i = 0 to (Array.length shiftX - 1)
  do
    result := bitwise_choice t shiftX.(i) !result (shift_left t (1 lsl i) !result);
  done;
  !result

let ( <<// ) t intY shiftX = shift_left' t shiftX intY

let card t bXarray = array_add t (Array.map uint_of_bool bXarray)

(* [exp_car t bXa =  (>>//) t (uint_one t ) (card t bXa)] *)
let exp_card t bXarray =
  Array.fold_left
    (fun result bX -> bitwise_choice t bX result (shift_left t 1 result))
    (uint_one t)
     bXarray

let scalar_binop_left binop bitX intY =
  map (binop bitX) intY

let scalar_binop_right binop intX bitY =
  map (fun bit -> binop bit bitY) intX

let ( |.&/ ) = scalar_binop_left ( &! )
let ( |.^/ ) = scalar_binop_left ( ^! )
let ( |.|/ ) = scalar_binop_left ( |! )
let ( |.=/ ) = scalar_binop_left ( =! )

let ( |&./ ) = scalar_binop_right ( &! )
let ( |^./ ) = scalar_binop_right ( ^! )
let ( ||./ ) = scalar_binop_right ( |! )
let ( |=./ ) = scalar_binop_right ( =! )

let lX (default:bool) (t:t) (intX:uint) (intY:uint) =
  let ( <! ) x y = (~! x) &! y in
  let length = max (Array.length intX) (Array.length intY) in
  let carry = ref (cst t default) in
  for i = 0 to length - 1
  do
    let x = uint_get t i intX
    and y = uint_get t i intY in
    carry := fite ((=!) x y) (x <! y) !carry;
  done;
  !carry

let lt = lX false
let ( </ ) = lt

let le = lX true
let ( <=/ ) = le

let gX default t intX intY = lX default t intY intX

let gt = gX false
let ( >/ ) = gt

let ge = gX true
let ( >=/ ) = ge

let argmax elim guard (uint:uint) : MLBDD.t * uint =
  lexmax_rev elim guard uint

type puint = f * uint

module ToS =
struct
  open STools.ToS
  open MlbddUtils.ToS

  let uint ?(man="man") (u:uint) : string =
    array (f ~man) u

  let puint ?(man="man") (p:puint) : string =
    ((f ~man) * (uint ~man)) p
end

module ToCir =
struct
  open STools.ToS
  open MlbddUtils.ToCir

  let string_of_pow2 (n:int) : string =
    assert(n >= 0);
    "b1"^(String.make n '0')

  let uint (u:uint) : string list =
    u
    |> Array.mapi (fun i c -> "(("^(f c)^") ? "^(string_of_pow2 i)^" : b0)")
    |> Array.to_list

  let puint (p:puint) : string list =
    let tail = uint (snd p) in
    if MLBDD.is_true (fst p)
    then tail
    else (("("^(f (fst p))^")")::(uint (snd p)))
end

module P =
struct
  let prefix = prefix ^ ".P"
  let sorted_support ((x, n):puint) : int list =
    Array.fold_left
      (fun s f -> SetList.union s (sorted_support f))
      (sorted_support x)
      n
  let support xn : MLBDD.support = MLBDD.support_of_list (sorted_support xn)

  let zero (man:MLBDD.man) : puint = (MLBDD.dtrue man, [||])
  let of_guard (g:MLBDD.t) : puint = (g, [||])
  let of_uint (man:MLBDD.man) (fn:uint) : puint = (MLBDD.dtrue man, fn)

  let ( +/ ) (t:t) ((x1, n1):puint) ((x2, n2):puint) : puint =
    (x1 &! x2, ( +/ ) t n1 n2)

  let addl (man:MLBDD.man) (xnl:puint list) : puint =
    match xnl with
    | [] -> zero man
    | h::t -> List.fold_left (( +/ ) man) h t

  let coproj_proj (elim:int list) ((x1, n1):puint) : MLBDD.t * puint =
    let supp = MLBDD.support_of_list elim in
    let (x1', n2) : puint = argmax x1 supp n1 in
    let x2 = MLBDD.exists supp x1 in
    (x1', (x2, n2))

  let is_trivial ((x, n):puint) : bool option =
    match is_trivial x with
    | None -> None
    | Some false -> Some false
    | Some true -> (
      if Array.length n = 0
      then Some true
      else None
    )
end

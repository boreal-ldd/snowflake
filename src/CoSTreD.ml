(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * Snowflake : A Generic Symbolic Dynamic Programming framework interfacing WAP, CoSTreD and MLBDD.
 *
 * CoSTreD :
 *   Functor Implementation of the CoSTreD (Reduced Block Triangular Form) algorithms :
 *     FRP : Forward Reduction Process
 *     BPP : Backward Propagation Process
*)

open GuaCaml
open Extra
open STools

type supp = int list

(* Section 1. Forward Reduction Process *)

module type MSigFRP =
sig
  type t (* representation language manager *)
  type f0 (* input representation language *)
  type f1 (* intermediary representation language *)
  type f2 (* output representation language *)

  val support0 : t -> f0 -> supp
  val support1 : t -> f1 -> supp
  val support2 : t -> f2 -> supp
  (* returns a sorted (super)set of the variables on which the function [f] depends *)
  val solve_variable : t -> int -> f0 list -> f1 list -> supp -> supp -> supp -> (f1 list) * f2
  (* [solve t n f0l f1l supp elim proj = (proj, coproj)] where :
      - NS := \union_i [fl]_i
      - [vl] \subset NS
      - [proj] is the projection of the conjunction fl on vl complement
      - [proj] is the result of existentially eliminating [elim] variables
      - [coproj] is the coprojection relatively to this projection operator
      - [fl] <> []
      - [n] : global arity
   *)
  (* [solve] guarentees that :
      - [support coproj] \subset NS
      - [support proj] \subset NS \setminus [vl]
      - [solve t [] vl = true * true]
   *)
  (* the boolean/exists case
     [solver t fl vl = (proj, coproj)] with
      coproj = /\_i vl_i
      proj = \exists vl. coproj
   *)

  (* val solve_parameter : t -> int -> f0 list -> f1 list -> supp -> (f0 list) * (f1 list) *)
  (* this method is called on remaining elements after FRP *)
  (* only usefull on the parametric case *)
  (* requires [solve t n [] = true] *)
  (*    - [n] : global arity *)
  (* [LATER] suboptimal on non-connected parameter variables *)

  val trivial0 : t -> f0 -> bool option
  val trivial1 : t -> f1 -> bool option
  val trivial2 : t -> f2 -> bool option
  (* [trivial t f = Some true ] :
        the constraint [f] is not relevant
        (e.g. (fun _ -> true) in the QBF case)
     [trivial t f = Some false] :
        the constraint [f] is contradictory
        (i.e. any system containing it has no solution)
        (e.g. (fun _ -> false) in the QBF case)
     [trivial t f = None      ] : the contraints [f] is not trivial
        (e.g. any non-constant function in the QBF case)
   *)

end

module type SigFRP =
sig
  module M : MSigFRP

  type f201 = (M.f2, M.f0 list * M.f1 list) AB.ab
    (* AA M.f2
       BB(M.f0 list * M.f1 list)
     *)

  (*  [apply t n fl wap_out = opa] where
        - [n] is the global arity of the problem
   *)
  val apply : M.t -> ?check:bool -> int -> M.f0 list -> M.f1 list -> Wap_exchange.output -> f201 array option
  (* Forward Reduction Process *)
end

module MakeFRP(M0:MSigFRP) : SigFRP
  with type M.t  = M0.t
  and  type M.f0  = M0.f0
  and  type M.f1  = M0.f1
  and  type M.f2  = M0.f2
=
struct
  module M = M0

  type f0 = M.f0 * supp
  type f1 = M.f1 * supp
  type f2 = M.f2 * supp

  type f201 = (M.f2, M.f0 list * M.f1 list) AB.ab
    (* A M.f2
       B(M.f0 list * M.f1 list)
     *)

  type state = f0 list * f1 list

  (* ga : global arity *)
  (* vl : list of variables to eliminate *)
  let frp_variable t ?(check=true) (ga:int) ((fl0, fl1):state) (supp:supp) (elim:supp) (proj:supp) : (M.f2 * state) option =
    let stop = OProfile.(time_start default) "[Snowflake.CoSTreD.FRP.frp_variable] verification" in
    assert(SetList.sorted supp);
    assert(SetList.sorted elim);
    assert(SetList.sorted proj);
    assert(SetList.union proj elim = supp);
    assert(SetList.nointer proj elim);
    stop();
    (* splitting according to support variables *)
    let stop = OProfile.(time_start default) "[Snowflake.CoSTreD.FRP.frp_variable] partitioning" in
    let nointer_elim (_, s) = SetList.nointer elim s in
    let fv0, fvc0 = MyList.partition nointer_elim fl0 in
    let fv1, fvc1 = MyList.partition nointer_elim fl1 in
    stop();
    (* computing projection, coprojection *)
    let stop = OProfile.(time_start default) "[Snowflake.CoSTreD.FRP.frp_variable] solving" in
    let pl, cop = M.solve_variable t ga
      (fv0||>fst) (fv1||>fst) supp elim proj in
    stop();
    (* checking compatibility with the new support *)
    let stop = OProfile.(time_start default) ("[Snowflake.CoSTreD.FRP.frp_variable] [check:"^(ToS.bool check)^"] support consistency") in
    if check
    then (
      List.iter (fun p ->
        let supp_p = M.support1 t p in
        assert(SetList.subset_of supp_p   proj)
      ) pl;
      let supp_cop = M.support2 t cop in
      assert(SetList.subset_of supp_cop supp);
    );
    stop();
    (* dealing with trivial projections *)
    let stop = OProfile.(time_start default) "[Snowflake.CoSTreD.FRP.frp_variable] post-processing" in
    let rec postloop carry = function
      | [] -> Some(List.rev carry)
      | p::pl' -> (
        match M.trivial1 t p with
        | Some false -> None
        | Some true  -> postloop carry pl'
        | None       -> postloop ((p, proj)::carry) pl'
      )
    in
    stop();
    match postloop fvc1 pl with
    | None       -> None
    | Some fvc1' -> Some(cop, (fvc0, fvc1'))

  (* ga : global arity
     vl : support parameters variables *)
  let frp_parameter t (ga:int) ((fl0, fl1):state) (supp:supp) : ((M.f0 list * M.f1 list) * state) option =
    (* splitting according to support variables *)
    let subset_supp (_, s) = SetList.subset_of s supp in
    let fvc0, fv0 = MyList.partition subset_supp fl0 in
    let fvc1, fv1 = MyList.partition subset_supp fl1 in
    Some ((fv0||>fst, fv1||>fst), (fvc0, fvc1))

  (* ga : global arity *)
  (* [carry] contains co-projections in reverse order *)
  let rec frp_iter t ?(check=true) (ga:int) (carry:f201 list) (st:state) (seq:Wap_exchange.output_component list) : f201 list option =
    match seq with
    | [] -> Some(List.rev carry)
    | comp::seq' -> (
      if comp.Wap_exchange.is_param
      then (
        let supp = comp.Wap_exchange.support in
        match frp_parameter t ga st supp with
        | None -> None
        | Some (lk, st') ->
          frp_iter t ~check ga ((AB.B lk)::carry) st' seq'
      )
      else (
        let supp = comp.Wap_exchange.support in
        let elim = comp.Wap_exchange.internal in
        let proj = comp.Wap_exchange.interface in
        match frp_variable t ~check ga st supp elim proj with
        | None -> None
        | Some (lf, st') ->
          frp_iter t ~check ga ((AB.A lf)::carry) st' seq'
      )
    )

  (* Forward Reduction Process *)
  (* ga : global arity *)
  let apply t ?(check=true) (ga:int) (fl0:M.f0 list) (fl1:M.f1 list) (out:Wap_exchange.output) : f201 array option =
    let stop = OProfile.(time_start default) "[Snowflake.CoSTreD.FRP.apply] support" in
    let fl0' = List.map (fun f -> (f, M.support0 t f)) fl0 in
    let fl1' = List.map (fun f -> (f, M.support1 t f)) fl1 in
    let seq = Array.to_list out.Wap_exchange.sequence in
    stop();
    match frp_iter t ~check ga [] (fl0', fl1') seq with
    | Some fl -> Some(Array.of_list fl)
    | None -> None

  let apply t ?(check=true) ga fl0 fl1 out : f201 array option =
    let stop = OProfile.(time_start default) "[Snowflake.CoSTreD.FRP.apply] total" in
    let opfa = apply t ~check ga fl0 fl1 out in
    stop();
    opfa
end

(* Section 2. Backward Propagation Process *)
module type MSigBPP =
sig
  type t (* representation language manager *)
  type f2 (* input representation language of functions *)
  type f3 (* output representation language of functions *)

  val support2 : t -> f2 -> supp
  val support3 : t -> f3 -> supp
  (*  [support t f = il] where
        [il] is the sorted (increasing order) (super)set of variables on which
        the function [f] depends
   *)
  (* val backsolve : t -> int -> (f * supp) -> f' *)
  (*  [backsolver t n (f, sf) = f']
        maps parameter variables to the output language
   *)

  val backproj : t -> int -> f3 -> f2 -> supp -> supp -> supp -> f3
  (*  [backproj t n f3 f2 supp3 inter supp2 = f3']
        project [f3] on [inter] and combine it with [f2]
        [f3]'s support is [supp3]
        [f2]'s support is [supp2]
   *)

end

module type SigBPP =
sig
  module M : MSigBPP

  type f23 = (M.f2, M.f3) AB.ab

  val apply : M.t -> int -> f23 array -> Wap_exchange.output -> M.f3 array
  (* Backward Propagation Process *)
end

module MakeBPP(M0:MSigBPP) : SigBPP
  with type M.t  = M0.t
  and  type M.f2 = M0.f2
  and  type M.f3 = M0.f3
=
struct
  module M = M0

  type f23 = (M.f2, M.f3) AB.ab

  (* ga : global arity *)
  let apply t (ga:int) (fa:f23 array) (out:Wap_exchange.output) : M.f3 array =
    let len = Array.length fa in
    let seq = out.Wap_exchange.sequence in
    assert(Array.length seq = len);
    (* initialize result array *)
    let res = Array.make len None in
    (* map parameter variables *)
    for i = len - 1 downto 0
    do
      let seq_i = seq.(i) in
      match seq_i.Wap_exchange.tree_pred with
      | Some pred -> (
        assert(seq_i.Wap_exchange.is_param = false);
        let f3 = Tools.unop res.(pred) in
        let supp3 = seq.(pred).Wap_exchange.support in
        let f2 = AB.unA fa.(i) in
        let supp2 = seq_i.Wap_exchange.support in
        let inter = SetList.inter supp2 supp3 in
        assert(SetList.subset_of seq_i.Wap_exchange.interface supp3);
        let stop = OProfile.(time_start default) "[Snowflake.CoSTreD.BPP.apply] backproj" in
        let    f3' = M.backproj t ga f3 f2 supp3 inter supp2 in
        stop();
        res.(i) <- Some f3';
      )
      | None -> (
        res.(i) <- Some (AB.unB fa.(i));
      )
    done;
    MyArray.map_unop res

  let apply t ga fa out : M.f3 array =
    let stop = OProfile.(time_start default) "[Snowflake.CoSTreD.BPP.apply] total" in
    let f'a = apply t ga fa out in
    stop();
    f'a
end

(* Section 3. Full Reduction (CoSTreD) *)
module type MSig =
sig
  type t (* representation language (RL) manager *)
  type f0 (* input RL *)
  type f1 (* pre-FRP  intermediary RL *)
  type f2 (* post-FRP intermediary RL *)
  type f3 (* post-BPP RL *)

  (* support *)
  val support0 : t -> f0 -> supp
  val support1 : t -> f1 -> supp
  val support2 : t -> f2 -> supp
  val support3 : t -> f3 -> supp

  (* trivial *)
  val trivial0 : t -> f0 -> bool option
  val trivial1 : t -> f1 -> bool option
  val trivial2 : t -> f2 -> bool option
  val trivial3 : t -> f3 -> bool option

  (* Section 1. FRP *)
  val solve_variable : t -> int -> f0 list -> f1 list -> supp -> supp -> supp -> (f1 list) * f2
  (* [solve t n f0l f1l supp elim proj = (proj, coproj)] *)

  (* Section 2.0 FRP to BPP *)
  val solve_parameter : t -> int -> f0 list -> f1 list -> supp -> f3

  (* Section 2. BPP *)
  val backproj : t -> int -> f3 -> f2 -> supp -> supp -> supp -> f3
  (*  [backproj t n f3 f2 supp3 inter supp2 = f3']
        project [f3] on [inter] and combine it with [f2]
        [f3]'s support is [supp3]
        [f2]'s support is [supp2]
   *)
end

module type Sig =
sig
  module M : MSig

  module FRP : SigFRP
    with type M.t  = M.t
    and  type M.f0  = M.f0
    and  type M.f1  = M.f1
    and  type M.f2  = M.f2

  module BPP : SigBPP
    with type M.t  = M.t
    and  type M.f2  = M.f2
    and  type M.f3  = M.f3

    (* (M.f2, M.f0 list * M.f1 list) AB.ab *)
  type f201 = FRP.f201

  (* Forward Reduction Process *)
  val apply_frp : M.t -> ?check:bool -> int -> M.f0 list -> M.f1 list -> Wap_exchange.output -> f201 array option

  type f23 = BPP.f23

  val f23a_of_f201a : M.t -> int -> f201 array -> Wap_exchange.output -> f23 array

  (* Backward Propagation Process *)
  val apply_bpp : M.t -> int -> f23 array -> Wap_exchange.output -> M.f3 array

  (* Full Reduction Process *)
  val apply : M.t -> ?check:bool -> int -> M.f0 list -> M.f1 list -> Wap_exchange.output -> (f201 array * f23 array * M.f3 array) option

  val apply_haltf3 :
     M.t -> ?check:bool -> int -> M.f0 list -> M.f1 list -> Wap_exchange.output ->
    (M.t -> M.f3 list -> 'res option) -> (* pre-BPP halting function *)
      (f201 array * f23 array * M.f3 array, 'res option) result
end

module Make(M0:MSig) : Sig
  with type M.t  = M0.t
  and  type M.f0 = M0.f0
  and  type M.f1 = M0.f1
  and  type M.f2 = M0.f2
  and  type M.f3 = M0.f3
=
struct
  module M = M0
  module FRP = MakeFRP(M)
  module BPP = MakeBPP(M)

  let apply_frp = FRP.apply
  let apply_bpp = BPP.apply

  type f201 = FRP.f201
  type f23  = BPP.f23

  let f23a_of_f201a t ga f201a out : f23 array =
    Array.map2 (fun f201 seq_i->
      match f201 with
      | AB.A lf -> (
        AB.A lf
      )
      | AB.B (f0l, f1l) -> (
        let supp = seq_i.Wap_exchange.support in
        AB.B (M.solve_parameter t ga f0l f1l supp)
      )
    ) f201a out.Wap_exchange.sequence

  (* [apply t ~check ga f0l f1l out]
     sequencially call the FRP and BPP processes
     by default ~check:true, if ~check:false turns off support consistency verification
   *)
  let apply t ?(check=true) ga f0l f1l out =
    match apply_frp t ~check ga f0l f1l out with
    | Some f201a ->
      let f23a = f23a_of_f201a t ga f201a out in
      let f3a = apply_bpp t ga f23a out in
      Some(f201a, f23a, f3a)
    | None -> None

  (* Similar to [apply] except that the functional parameter [halt] is called
     after the FRP and before the BPP
     [halt t f3l] is evaluated with [t:M.t] the manager and [f3l:f3 list] the
     list of [f3] term in the result of the FRP after the terminal reduction of
     [f0 list * f1 list]
     in particular:
     -            if the FRP returns None, [apply_haltf3] returns [Error None]
     - otherwise, if halt returns Some res, returns[Error(Some res)]
     - otherwise, compute BPP and returns Ok(apply t ga f0l f1l out)
     by default ~check:true, if ~check:false turns off support consistency verification
   *)
  let apply_haltf3 t ?(check=true) ga f0l f1l out (halt:M.t -> M.f3 list -> 'res option) :
      (f201 array * f23 array * M.f3 array, 'res option) result =
    match apply_frp t ~check ga f0l f1l out with
    | Some f201a -> (
      let f23a : (M.f2, M.f3) AB.ab array = f23a_of_f201a t ga f201a out in
      let f3l : M.f3 list = AB.filterB_array f23a in
      match halt t f3l with
      | Some res -> Error(Some res)
      | None -> (
        let f3a = apply_bpp t ga f23a out in
        Ok(f201a, f23a, f3a)
      )
    )
    | None -> Error None

end

(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * Snowflake : A Generic Symbolic Dynamic Programming framework interfacing WAP, CoSTreD and MLBDD.
 *
 * Wap_exchange : describes the interface formats between CoSTreD and WAP
*)

type input = {
  variables : int list; (* set of suppressibles vertices *)
  parameters: int list; (* set of unsuppressible vertices *)
  kdecomp   : int list list; (* describes the graphs as a sum of clique (each [int list] element) *)
}
(* typically if your system is described as a set of equation
   the primal graph of your system can be described by adding
   a clique for each equation containing the variables appearing in it.
   For example the following pseudo-code computes a valid primal graph :
   [List.map (fun equation -> 'list of variables appearing in' equation) equations]
 *)

type proto_solver = input -> int list list * int list

type output_component = {
  index           : int;        (* index of the current component *)
  is_param        : bool;       (* [is_param = true ] this is a parameter type component
                                   [is_param = false] this is a variable  type component *)
  support         : int list;   (* neighborhood of the component *)
  internal        : int list;   (* set of internal vertices (i.e. the one eliminated by the current component) *)
  interface       : int list;   (* strict-neighborhood *)
  var_interface   : int list;   (* supressible strict-neighborhood *)
  param_interface : int list;   (* unsuppressible strict-neighborhood *)
  tree_pred       : int option; (* parent tree-node/component if any *)
}
(* remark: relational set properties between fields

  support = internal \sqcup interface
  internal \subset input.variables
  interface = var_interface \sqcup param_interface (* \sqcup = disjoint union *)
  var_interface = interface \union input.variables
  param_interface = interface \union input.parameters
  tree_pred = Some j -> j > index
 *)

(* variable type components *)
(*
type var_component = {
  index           : int;        (* index of the current component *)
  is_param        = false;
  support         : int list;   (* neighborhood of the component *)
  internal        : int list;   (* set of internal vertices (i.e. the one eliminated by the current component) *)
  interface       : int list;   (* strict-neighborhood *)
  var_interface   : int list;   (* supressible strict-neighborhood *)
  param_interface : int list;   (* unsuppressible strict-neighborhood *)
  tree_pred       = Some int;   (* parent tree-node/component if any *)
}
*)

(* parameter type components *)
(*
type output_component = {
  index           : int;        (* index of the current component *)
  is_param        = true
  support         : int list;   (* the param-type component *)
  internal        = [];         (* = empty (there is no variable left to eliminate) *)
  interface       = [];         (* = empty (may other param-type components, but not relevant) *)
  var_interface   = [];         (* = empty (there is no variable interface, they all have been suppressed) *)
  param_interface = support;    (* = neighborhood (there is only parameter variables *)
  tree_pred       = None        (* param-type component do not have ancestors *)
}
 *)

type sequence = output_component array
  (* [sequence] : describes the suppression sequence with relevant annotations *)

type output = {
  input : input;
  (* [input] : pointer to the input object *)
  sequence : sequence;
  (* [sequence] : describes the suppression sequence with relevant annotations *)
  cost : int list;
  (* [cost] represents the WAP-cost of the [sequence]
     [cost] is represented using [bicimal] representation type *)
  optimal : bool;
  (* [optimal] is [true] if the sequence is proved optimal, [false] otherwise *)
  (* in particular [optimal = false] does not imply that the sequence is suboptimal,
     only that one could not prove it (e.g. time/memory restriction) *)
}

type solver = input -> output

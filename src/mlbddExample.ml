(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * Snowflake : A Generic Symbolic Dynamic Programming framework interfacing WAP, CoSTreD and MLBDD.
 *
 * MlbddExample : Example of Usage of the CoSTreD functor
 *)

open GuaCaml
open Extra
open MlbddUtils
open MlbddUInt

let prefix = "Snowflake.MlbddExample"

module AndL =
struct
  let prefix = prefix ^ ".AndL"

  (* Section 3. Full Reduction (CoSTreD) *)
  module Modele =
  struct
    type t = MLBDD.man
    type f0 = MLBDD.t (* input RL *)
    type f1 = MLBDD.t (* pre-FRP  intermediary RL *)
    type f2 = MLBDD.t (* post-FRP intermediary RL *)
    type f3 = MLBDD.t (* post-BPP RL *)

    (* support *)
    let support0 _ f = sorted_support f
    let support1 = support0
    let support2 = support0
    let support3 = support0

    (* trivial *)
    let trivial0 _ f =
           if MLBDD.is_true  f then Some true
      else if MLBDD.is_false f then Some false
      else None
    let trivial1 = trivial0
    let trivial2 = trivial0
    let trivial3 = trivial0

    type supp = int list

    (* Section 1. FRP *)
    (*  solve_variable (man:t) (ga:int) (f0l:f0 list) (f1l:f1 list) (supp:supp) (elim:supp) (proj:supp) : (f1 list) * f2 *)
    let solve_variable (man:t) (ga:int) (f0l:f0 list) (f1l:f1 list) ( _  :supp) (elim:supp) ( _  :supp) : (f1 list) * f2 =
      let f0 = List.fold_left MLBDD.dand (MLBDD.dtrue man) f0l in
      let cp = List.fold_left MLBDD.dand f0 f1l in
      let p = MLBDD.exists (MLBDD.support_of_list elim) cp in
      ([p], cp)

    (* [solve t n f0l f1l supp elim proj = (proj, coproj)] *)

    (* Section 2.0 FRP to BPP *)
    (*  solve_parameter (man:t) (ga:int) (f0l:f0 list) (f1l:f1 list) (supp:supp) : f3 *)
    let solve_parameter (man:t) (ga:int) (f0l:f0 list) (f1l:f1 list) (supp:supp) : f3 =
      let f0 = List.fold_left MLBDD.dand (MLBDD.dtrue man) f0l in
      let f1 = List.fold_left MLBDD.dand f0 f1l in
      f1

    (* Section 2. BPP *)
    (*  backproj (man:t) (ga:int) (f3:f3) (f2:f2) (supp3:supp) (inter:supp) (supp2:supp) : f3 *)
    let backproj (man:t) (ga:int) (f3:f3) (f2:f2) (supp3:supp) (inter:supp) (supp2:supp) : f3 =
      let elim = SetList.minus supp3 inter in (* SetList.minus supp3 supp2 *)
      let f3' = MLBDD.exists (MLBDD.support_of_list elim) f3 in
      (MLBDD.dand f3' f2)
  end

  module Module = CoSTreD.Make(Modele)

  let wap_of_system (man:MLBDD.man) (parameters:int list) (fl:MLBDD.t list) : Wap_exchange.input =
    let kdecomp : int list list = fl ||> sorted_support in
    let variables = SetList.union_list kdecomp in
    (* we remove parameter variables from suppressible variables *)
    let variables = SetList.minus variables parameters in
    let wap = Wap_exchange.{variables; parameters; kdecomp} in
    (* checks that wap's input is well formed *)
    Wap_exchange_utils.assert_input wap;
    wap

  type wap_solver = Wap_exchange.input -> Wap_exchange.output

  let default_wap_solver : wap_solver =
    Wap_lightspeed.lightspeed_greedy

  let compute
     ?(verbose=0)
     ?(export_CS=(None:string option))
     ?(export_graphviz=(None:string option))
     ?(wap_solver=default_wap_solver)
     ?(support_consistency=true)
      (man:MLBDD.man)
     ?(ninput=None)
      (parameters:int list)
      (fl:MLBDD.t list) : MLBDD.t =
    let prefix = "["^prefix^".compute]" in
    let parameters = SetList.sort parameters in
    let stop = OProfile.(time_start default (prefix^" wap_building")) in
    let wap_in = wap_of_system man parameters fl in
    stop();
    if verbose >= 2 then print_endline (prefix^" wap_in:"^(Wap_exchange_utils.ToS.input wap_in));
    (match export_CS with
      | None -> ()
      | Some target -> (
        Wap_exchange_utils.cir_file_of_boolean_constraint_system
          ~ninput
           wap_in.Wap_exchange.variables
           wap_in.Wap_exchange.parameters
           fl
           target
      )
    );
    (match export_graphviz with
      | None -> ()
      | Some target -> (
        let gom = Wap_exchange_utils.GOM_CFT in
        Wap_exchange_utils.to_graphviz_file ~name:"AndL" ~gom target wap_in
      )
    );
    let stop = OProfile.(time_start default (prefix^" wap_solving")) in
    let out_wap = wap_solver wap_in in
    stop();
    if verbose >= 2 then print_endline (prefix^" out_wap:"^(Wap_exchange_utils.ToS.output out_wap));
    let stop = OProfile.(time_start default (prefix^" CoSTreD_call")) in
    let opr = Module.apply man ~check:support_consistency (-1) fl [] out_wap in
    stop();
    match opr with
    | None -> MLBDD.dfalse man
    | Some(_, _, out_bpp) -> (
      let stop = OProfile.(time_start default (prefix^" big_and")) in
      let f = lexand man (Array.to_list out_bpp) in
      stop();
      if verbose >= 1 then OProfile.(print_table default);
      f
    )
end

module Argmax_UInt =
struct
  let prefix = prefix ^ ".Argmax_Uint"

  (* Section 3. Full Reduction (CoSTreD) *)
  module Modele =
  struct
    type t = MLBDD.man
    (* input RL *)
    type f0 = MlbddUInt.puint
    (* pre-FRP  intermediary RL *)
    type f1 = MlbddUInt.puint
    (* post-FRP intermediary RL *)
    type f2 = MLBDD.t
    (* post-BPP RL *)
    type f3 = MLBDD.t

    (* support *)
    let support0 _ xn = P.sorted_support xn
    let support1 _ xn = P.sorted_support xn
    let support2 _ f = sorted_support f
    let support3 _ f = sorted_support f

    (* trivial *)
    let trivial0 _ f = P.is_trivial f
    let trivial1 _ f = P.is_trivial f
    let trivial2 _ f = is_trivial f
    let trivial3 _ f = is_trivial f

    type supp = int list

    (* Section 1. FRP *)
    (*  solve_variable (man:t) (ga:int) (f0l:f0 list) (f1l:f1 list) (supp:supp) (elim:supp) (proj:supp) : (f1 list) * f2 *)
    let solve_variable (man:t) (ga:int) (f0l:f0 list) (f1l:f1 list) ( _  :supp) (elim:supp) ( _  :supp) : (f1 list) * f2 =
      let tot = P.addl man (f0l @> f1l) in
      let cp, p = P.coproj_proj elim tot in
      ([p], cp)

    (* [solve t n f0l f1l supp elim proj = (proj, coproj)] *)

    (* Section 2.0 FRP to BPP *)
    (*  solve_parameter (man:t) (ga:int) (f0l:f0 list) (f1l:f1 list) (supp:supp) : f3 *)
    let solve_parameter (man:t) (ga:int) (f0l:f0 list) (f1l:f1 list) (supp:supp) : f3 =
      let tot = P.addl man (f0l @> f1l) in
      fst tot

    (* Section 2. BPP *)
    (*  backproj (man:t) (ga:int) (f3:f3) (f2:f2) (supp3:supp) (inter:supp) (supp2:supp) : f3 *)
    let backproj (man:t) (ga:int) (f3:f3) (f2:f2) (supp3:supp) (inter:supp) (supp2:supp) : f3 =
      let elim = SetList.minus supp3 inter in (* SetList.minus supp3 supp2 *)
      let f3' = MLBDD.exists (MLBDD.support_of_list elim) f3 in
      (MLBDD.dand f3' f2)
  end

  module Module = CoSTreD.Make(Modele)

  let wap_of_system (man:MLBDD.man) (parameters:int list) (fl:MlbddUInt.puint list) : Wap_exchange.input =
    let kdecomp : int list list = fl ||> P.sorted_support in
    let variables = SetList.union_list kdecomp in
    (* we remove parameter variables from suppressible variables *)
    let variables = SetList.minus variables parameters in
    let wap = Wap_exchange.{variables; parameters; kdecomp} in
    (* checks that wap's input is well formed *)
    Wap_exchange_utils.assert_input wap;
    wap

  type wap_solver = Wap_exchange.input -> Wap_exchange.output

  let default_wap_solver : wap_solver =
    Wap_lightspeed.lightspeed_greedy

  (* returns [None] if the formula is false
     returns [Some _ ] otherwise *)
  let compute
     ?(verbose=0)
     ?(export_CS=(None:string option)) (* [ ~export_CS:(Some file_name) ] export the constraint system using [ToS] modules into the file [file_name] *)
     ?(export_graphviz=(None:string option)) (* [ ~export_graphviz:(Some file_name) ] export the primal graph of the constraint system GuaCaml's module for graphviz export *)
     ?(wap_solver=default_wap_solver)
     ?(halt_bpp=((fun _ _ -> None):(Modele.t -> Modele.f3 list -> 'res option)))
     ?(support_consistency=true)
     (* if stop_frp (if specified) returns [Some res], halts the computation after FRP and returns (Error res)
        otherwise continue with BPP *)
      (man:MLBDD.man)
     ?(ninput=None)
      (parameters:int list)
      (fl:MlbddUInt.puint list) : (MLBDD.t, 'res option) result =
    let prefix = "["^prefix^".compute]" in
    let parameters = SetList.sort parameters in
    let stop = OProfile.(time_start default (prefix^" wap_building")) in
    let wap_in = wap_of_system man parameters fl in
    stop();
    if verbose >= 2 then print_endline (prefix^" wap_in:"^(Wap_exchange_utils.ToS.input wap_in));
    (match export_CS with
      | None -> ()
      | Some target -> (
        Wap_exchange_utils.cir_file_of_puint_constraint_system
          ~ninput
           wap_in.Wap_exchange.variables
           wap_in.Wap_exchange.parameters
           fl
           target
      )
    );
    (match export_graphviz with
      | None -> ()
      | Some target -> (
        let gom = Wap_exchange_utils.GOM_CFT in
        Wap_exchange_utils.to_graphviz_file ~name:"Argmax_UInt" ~gom target wap_in
      )
    );
    let stop = OProfile.(time_start default (prefix^" wap_solving")) in
    let out_wap = wap_solver wap_in in
    stop();
    if verbose >= 2 then print_endline (prefix^" out_wap:"^(Wap_exchange_utils.ToS.output out_wap));
    let stop = OProfile.(time_start default (prefix^" CoSTreD_call")) in
    let opr = Module.apply_haltf3 man ~check:support_consistency (-1) fl [] out_wap halt_bpp in
    stop();
    match opr with
    | Error value -> Error value
    | Ok(_, _, out_bpp) -> (
      let stop = OProfile.(time_start default (prefix^" big_and")) in
      let f = lexand man (Array.to_list out_bpp) in
      stop();
      if verbose >= 1 then OProfile.(print_table default);
      Ok f
    )
end

module Argmax_UInt_Select =
struct
  let prefix = prefix ^ ".Argmax_UInt_Select"

  (* Section 3. Full Reduction (CoSTreD) *)
  module Modele =
  struct
    let prefix = prefix ^ ".Modele"
    type t = {
      bdd_man : MLBDD.man;
      param_v : int list
    }
    (* input RL *)
    type f0 = MlbddUInt.puint
    (* pre-FRP  intermediary RL *)
    type f1 = MlbddUInt.puint
    (* post-FRP intermediary RL *)
    type f2 = MLBDD.t
    (* post-BPP RL *)
    type f3 = psubst * MLBDD.t

    (* support *)
    let support0 _ xn = P.sorted_support xn
    let support1 _ xn = P.sorted_support xn
    let support2 _ f = sorted_support f
    let support3 _ (xs, fs) =
      SetList.union (PSubst.sorted_support xs) (sorted_support fs)

    (* trivial *)
    let trivial0 _ f = P.is_trivial f
    let trivial1 _ f = P.is_trivial f
    let trivial2 _ f = is_trivial f
    let trivial3 _ ((inv, fl), fs) =
      is_trivial fs

    type supp = int list

    (* Section 1. FRP *)
    (*  solve_variable (t:t) (ga:int) (f0l:f0 list) (f1l:f1 list) (supp:supp) (elim:supp) (proj:supp) : (f1 list) * f2 *)
    let solve_variable (t:t) (ga:int) (f0l:f0 list) (f1l:f1 list) (supp:supp) (elim:supp) (proj:supp) : (f1 list) * f2 =
      (* let prefix = "["^prefix^".solve_variable]" in *)
      (* let stop = OProfile.(time_start default (prefix^" P.addl")) in *)
      let tot = P.addl t.bdd_man (f0l @> f1l) in
      (* stop(); *)
      (* let stop = OProfile.(time_start default (prefix^" P.coproj_proj")) in *)
      let cp, p = P.coproj_proj elim tot in
      (* stop(); *)
      (* assert(SetList.subset_of (sorted_support cp) supp); *)
      (* assert(SetList.subset_of (P.sorted_support p) proj); *)
      ([p], cp)

    (* [solve t n f0l f1l supp elim proj = (proj, coproj)] *)

    (* Section 2.0 FRP to BPP *)
    (*  solve_parameter (t:t) (ga:int) (f0l:f0 list) (f1l:f1 list) (supp:supp) : f3 *)
    let solve_parameter (t:t) (ga:int) (f0l:f0 list) (f1l:f1 list) (supp:supp) : f3 =
      let tot = P.addl t.bdd_man (f0l @> f1l) in
      (* assert(SetList.subset_of (P.sorted_support tot) t.param_v); *)
      ((fst tot, []), fst tot)

    (* Section 2. BPP *)
    (*  backproj (t:t) (ga:int) (f3:f3) (f2:f2) (supp3:supp) (inter:supp) (supp2:supp) : f3 *)
    let backproj (t:t) (ga:int) ((xs', fs'):f3) (cp:f2) (xs'_supp:supp) (inter:supp) (cp_supp:supp) : f3 =
      (* let prefix = "["^prefix^".backproj]" in *)
      (* assert(SetList.subset_of (sorted_support cp) cp_supp); *)
      (* assert(SetList.subset_of (PSubst.sorted_support xs') (SetList.union xs'_supp t.param_v)); *)
      (* assert(SetList.subset_of (PSubst.sorted_domain xs') t.param_v); *)
      (* assert(SetList.subset_of (sorted_support fs') (SetList.union xs'_supp t.param_v)); *)
      (* advanced consistency verification *)
      (* let var3 = SetList.minus xs'_supp t.param_v in *)
      (* let var2 = SetList.minus cp_supp t.param_v in *)
      (* assert(PSubst.equal xs' (exists_proj fs' var3)); *)

      (* cp  : is the local solution space *)
      (* fs' : is the selected solution space of the ancestor *)
      (* local_sol : is the intersection between cp and fs' restricted to supp2 *)
      let local_sol =
        let elim =
          SetList.minus xs'_supp (SetList.union cp_supp t.param_v)
          |> MLBDD.support_of_list
        in
        (* we compute the local solution space by intersection *)
         MLBDD.dand cp (MLBDD.exists elim fs')
      in
      (* intern_sol : is the projection of f3'2 to (supp2 - supp3) + param *)
      let intern_sol =
        let elim =
          SetList.minus (SetList.inter xs'_supp cp_supp) t.param_v
          |> MLBDD.support_of_list
        in
        MLBDD.exists elim local_sol
      in
      (* [DEBUG] we check that the transformation is equivalent to injecting local projections *)
      (* assert(MLBDD.equal intern_sol (PSubst.subst cp xs')); (* [DEBUG] *) *)
      (* we compute the relevant set of variables for selection and projection *)
      let sorted_elim = SetList.minus cp_supp (SetList.union xs'_supp t.param_v) in
      (* intern_selected : sat_select intern_sol *)
      let intern_selected =
        let inv, guard = sat_select intern_sol sorted_elim in
        assert(MLBDD.equal inv (fst xs'));
        guard
      in
      (* fs : local selected solution space *)
      let fs = MLBDD.dand local_sol intern_selected in
      (* xs_selected : computing internal projections *)
      let xs_selected : psubst =
        let xs = exists_proj intern_selected sorted_elim in
        assert(MLBDD.equal (fst xs) (fst xs'));
        xs
      in
      (* [DEBUG] we check that projections are properly computed *)
      (* assert(PSubst.equal xs_selected (PSubst.of_guard intern_sol sorted_elim)); *)
      (* xs : combining internal and ancestor projection to obtain local projections *)
      let snd_xs_1 = Assoc.update_with ~noconflict:true (snd xs_selected) (snd xs') in
      let snd_xs = Assoc.restr snd_xs_1 cp_supp in
      let xs = (fst xs', snd_xs) in
      (* assert(PSubst.equal xs (exists_proj fs var2)); (* [DEBUG] *) *)
      (xs, fs)
  end

  module Module = CoSTreD.Make(Modele)

  let wap_of_system (man:MLBDD.man) (parameters:int list) (fl:MlbddUInt.puint list) : Wap_exchange.input =
    let kdecomp : int list list = fl ||> P.sorted_support in
    let variables = SetList.union_list kdecomp in
    (* we remove parameter variables from suppressible variables *)
    let variables = SetList.minus variables parameters in
    let wap = Wap_exchange.{variables; parameters; kdecomp} in
    (* checks that wap's input is well formed *)
    Wap_exchange_utils.assert_input wap;
    wap

  type wap_solver = Wap_exchange.input -> Wap_exchange.output

  let default_wap_solver : wap_solver =
    Wap_lightspeed.lightspeed_greedy

  let compute
     ?(verbose=0)
     ?(export_CS=(None:string option)) (* [ ~export_CS:(Some file_name) ] export the constraint system using [ToS] modules into the file [file_name] *)
     ?(export_graphviz=(None:string option))
     ?(wap_solver=default_wap_solver)
     ?(halt_bpp=((fun _ _ -> None):(MLBDD.man -> MLBDD.t list -> 'res option)))
     ?(support_consistency=true)
     (* if stop_frp (if specified) returns [Some res], halts the computation after FRP and returns (Error res)
        otherwise continue with BPP *)
      (man:MLBDD.man)
     ?(ninput=None)
      (parameters:int list)
      (fl:MlbddUInt.puint list) : ((MLBDD.var * MLBDD.t)list, 'res option) result =
    let prefix = "["^prefix^".compute]" in
    let parameters = SetList.sort parameters in
    let stop = OProfile.(time_start default (prefix^" wap_building")) in
    let wap_in = wap_of_system man parameters fl in
    stop();
    if verbose >= 2 then print_endline (prefix^" wap_in:"^(Wap_exchange_utils.ToS.input wap_in));
    (match export_CS with
      | None -> ()
      | Some target -> (
        Wap_exchange_utils.cir_file_of_puint_constraint_system
          ~ninput
           wap_in.Wap_exchange.variables
           wap_in.Wap_exchange.parameters
           fl
           target
      )
    );
    (match export_graphviz with
      | None -> ()
      | Some target -> (
        let gom = Wap_exchange_utils.GOM_CFT in
        Wap_exchange_utils.to_graphviz_file ~name:"Argmax_UInt_Select" ~gom target wap_in
      )
    );
    let stop = OProfile.(time_start default (prefix^" wap_solving")) in
    let out_wap = wap_solver wap_in in
    stop();
    if verbose >= 2 then print_endline (prefix^" out_wap:"^(Wap_exchange_utils.ToS.output out_wap));
    let stop = OProfile.(time_start default (prefix^" CoSTreD_call")) in
    let t = Modele.{bdd_man = man; param_v = parameters} in
    let opr = Module.apply_haltf3 t ~check:support_consistency (-1) fl [] out_wap (fun t ifl -> halt_bpp t.Modele.bdd_man (ifl ||> fst ||> fst)) in
    stop();
    match opr with
    | Error value -> Error value
    | Ok(_, _, out_bpp) -> (
      let stop = OProfile.(time_start default (prefix^" gathering")) in
      let fl = Array.fold_left (fun carry ((_, fl), _) -> Assoc.update_with carry fl) [] out_bpp in
      stop();
      if verbose >= 1 then OProfile.(print_table default);
      Ok fl
    )
end

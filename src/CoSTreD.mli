(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * Snowflake : A Generic Symbolic Dynamic Programming framework interfacing WAP, CoSTreD and MLBDD.
 *
 * CoSTreD :
 *   Functor Implementation of the CoSTreD (Reduced Block Triangular Form) algorithms :
 *     FRP : Forward Reduction Process
 *     BPP : Backward Propagation Process
*)

open GuaCaml
open Extra
open STools

type supp = int list

(* Section 1. Forward Reduction Process *)

module type MSigFRP =
sig
  type t (* representation language manager *)
  type f0 (* input representation language *)
  type f1 (* intermediary representation language *)
  type f2 (* output representation language *)

  val support0 : t -> f0 -> supp
  val support1 : t -> f1 -> supp
  val support2 : t -> f2 -> supp
  (* returns a sorted (super)set of the variables on which the function [f] depends *)
  val solve_variable : t -> int -> f0 list -> f1 list -> supp -> supp -> supp -> (f1 list) * f2
  (* [solve t n f0l f1l supp elim proj = (proj, coproj)] where :
      - NS := \union_i [fl]_i
      - [vl] \subset NS
      - [proj] is the projection of the conjunction fl on vl complement
      - [proj] is the result of existentially eliminating [elim] variables
      - [coproj] is the coprojection relatively to this projection operator
      - [fl] <> []
      - [n] : global arity
   *)
  (* [solve] guarentees that :
      - [support coproj] \subset NS
      - [support proj] \subset NS \setminus [vl]
      - [solve t [] vl = true * true]
   *)
  (* the boolean/exists case
     [solver t fl vl = (proj, coproj)] with
      coproj = /\_i vl_i
      proj = \exists vl. coproj
   *)

  (* val solve_parameter : t -> int -> f0 list -> f1 list -> supp -> (f0 list) * (f1 list) *)
  (* this method is called on remaining elements after FRP *)
  (* only usefull on the parametric case *)
  (* requires [solve t n [] = true] *)
  (*    - [n] : global arity *)
  (* [LATER] suboptimal on non-connected parameter variables *)

  val trivial0 : t -> f0 -> bool option
  val trivial1 : t -> f1 -> bool option
  val trivial2 : t -> f2 -> bool option
  (* [trivial t f = Some true ] :
        the constraint [f] is not relevant
        (e.g. (fun _ -> true) in the QBF case)
     [trivial t f = Some false] :
        the constraint [f] is contradictory
        (i.e. any system containing it has no solution)
        (e.g. (fun _ -> false) in the QBF case)
     [trivial t f = None      ] : the contraints [f] is not trivial
        (e.g. any non-constant function in the QBF case)
   *)

end

module type SigFRP =
sig
  module M : MSigFRP

  type f201 = (M.f2, M.f0 list * M.f1 list) AB.ab
    (* GLeaf M.f2
       GLink(M.f0 list * M.f1 list)
     *)

  (*  [apply t n fl wap_out = opa] where
        - [n] is the global arity of the problem
   *)
  val apply : M.t -> ?check:bool -> int -> M.f0 list -> M.f1 list -> Wap_exchange.output -> f201 array option
  (* Forward Reduction Process *)
end

module MakeFRP(M0:MSigFRP) : SigFRP
  with type M.t  = M0.t
  and  type M.f0  = M0.f0
  and  type M.f1  = M0.f1
  and  type M.f2  = M0.f2

(* Section 2. Backward Propagation Process *)
module type MSigBPP =
sig
  type t (* representation language manager *)
  type f2 (* input representation language of functions *)
  type f3 (* output representation language of functions *)

  val support2 : t -> f2 -> supp
  val support3 : t -> f3 -> supp
  (*  [support t f = il] where
        [il] is the sorted (increasing order) (super)set of variables on which
        the function [f] depends
   *)
  (* val backsolve : t -> int -> (f * supp) -> f' *)
  (*  [backsolver t n (f, sf) = f']
        maps parameter variables to the output language
   *)

  val backproj : t -> int -> f3 -> f2 -> supp -> supp -> supp -> f3
  (*  [backproj t n f3 f2 supp3 inter supp2 = f3']
        project [f3] on [inter] and combine it with [f2]
        [f3]'s support is [supp3]
        [f2]'s support is [supp2]
   *)

end

module type SigBPP =
sig
  module M : MSigBPP

  type f23 = (M.f2, M.f3) AB.ab

  val apply : M.t -> int -> f23 array -> Wap_exchange.output -> M.f3 array
  (* Backward Propagation Process *)
end

module MakeBPP(M0:MSigBPP) : SigBPP
  with type M.t  = M0.t
  and  type M.f2 = M0.f2
  and  type M.f3 = M0.f3

(* Section 3. Full Reduction (CoSTreD) *)
module type MSig =
sig
  type t (* representation language (RL) manager *)
  type f0 (* input RL *)
  type f1 (* pre-FRP  intermediary RL *)
  type f2 (* post-FRP intermediary RL *)
  type f3 (* post-BPP RL *)

  (* support *)
  val support0 : t -> f0 -> supp
  val support1 : t -> f1 -> supp
  val support2 : t -> f2 -> supp
  val support3 : t -> f3 -> supp

  (* trivial *)
  val trivial0 : t -> f0 -> bool option
  val trivial1 : t -> f1 -> bool option
  val trivial2 : t -> f2 -> bool option
  val trivial3 : t -> f3 -> bool option

  (* Section 1. FRP *)
  val solve_variable : t -> int -> f0 list -> f1 list -> supp -> supp -> supp -> (f1 list) * f2
  (* [solve t n f0l f1l supp elim proj = (proj, coproj)] *)

  (* Section 2.0 FRP to BPP *)
  val solve_parameter : t -> int -> f0 list -> f1 list -> supp -> f3

  (* Section 2. BPP *)
  val backproj : t -> int -> f3 -> f2 -> supp -> supp -> supp -> f3
  (*  [backproj t n f3 f2 supp3 inter supp2 = f3']
        project [f3] on [inter] and combine it with [f2]
        [f3]'s support is [supp3]
        [f2]'s support is [supp2]
   *)
end

module type Sig =
sig
  module M : MSig

  module FRP : SigFRP
    with type M.t  = M.t
    and  type M.f0  = M.f0
    and  type M.f1  = M.f1
    and  type M.f2  = M.f2

  module BPP : SigBPP
    with type M.t  = M.t
    and  type M.f2  = M.f2
    and  type M.f3  = M.f3

    (* (M.f2, M.f0 list * M.f1 list) AB.ab *)
  type f201 = FRP.f201

  (* Forward Reduction Process *)
  val apply_frp : M.t -> ?check:bool -> int -> M.f0 list -> M.f1 list -> Wap_exchange.output -> f201 array option

  type f23 = BPP.f23

  val f23a_of_f201a : M.t -> int -> f201 array -> Wap_exchange.output -> f23 array

  (* Backward Propagation Process *)
  val apply_bpp : M.t -> int -> f23 array -> Wap_exchange.output -> M.f3 array

  (* Full Reduction Process *)
  (* [apply t ~check ga f0l f1l out]
     sequencially call the FRP and BPP processes
     by default ~check:true, if ~check:false turns off support consistency verification
   *)
  val apply : M.t -> ?check:bool -> int -> M.f0 list -> M.f1 list -> Wap_exchange.output -> (f201 array * f23 array * M.f3 array) option

  (* Similar to [apply] except that the functional parameter [halt] is called
     after the FRP and before the BPP
     [halt t f3l] is evaluated with [t:M.t] the manager and [f3l:f3 list] the
     list of [f3] term in the result of the FRP after the terminal reduction of
     [f0 list * f1 list]
     in particular:
     -            if the FRP returns None, [apply_haltf3] returns [Error None]
     - otherwise, if halt returns Some res, returns[Error(Some res)]
     - otherwise, compute BPP and returns Ok(apply t ga f0l f1l out)
     by default ~check:true, if ~check:false turns off support consistency verification
   *)
  val apply_haltf3 :
     M.t -> ?check:bool -> int -> M.f0 list -> M.f1 list -> Wap_exchange.output ->
    (M.t -> M.f3 list -> 'res option) -> (* pre-BPP halting function *)
      (f201 array * f23 array * M.f3 array, 'res option) result
end

module Make(M0:MSig) : Sig
  with type M.t  = M0.t
  and  type M.f0 = M0.f0
  and  type M.f1 = M0.f1
  and  type M.f2 = M0.f2
  and  type M.f3 = M0.f3

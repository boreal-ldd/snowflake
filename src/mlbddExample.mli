(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * Snowflake : A Generic Symbolic Dynamic Programming framework interfacing WAP, CoSTreD and MLBDD.
 *
 * MlbddExample : Example of Usage of the CoSTreD functor
 *)

open GuaCaml

module AndL :
sig
  type wap_solver = Wap_exchange.input -> Wap_exchange.output

  (* [wap_of_system man param funlist = wap_input] where:
   *  - [man : MLBDD.man], is the MLBDD manager of the function in [funlist]
   *  - [param : int list], is the set of mode variables sorted in increasing order
   *  - [funlist : MLBDD.t list], is the set of Boolean Function which are to be conjuncted
   *  - [wap_input : Wap_exchange.input], is the WAP modelisation of this system
   *)
  val wap_of_system : MLBDD.man -> int list -> MLBDD.t list -> Wap_exchange.input

  (* let default_wap_solver : wap_solver = Wap_greedy.lightweight_greedy *)
  val default_wap_solver : wap_solver

  (* [compute
        ~verbose:0
        ~wap_solver=default_wap_solver
        ~support_consistency:true
        ~export_CS:None
        ~export_graphviz:None
          man param funlist = solved] where:
   *  - [man : MLBDD.man], is the MLBDD manager of the function in [funlist]
   *  - [param : int list], is the set of mode variables sorted in increasing order
   *  - [funlist : MLBDD.t list], is the set of Boolean Function which are to be conjuncted
   *  - [solved : MLBDD.t], is the conjunction of the Boolean Functions in [funlist]
   *
   * === NOTE ===
   * verbose = 0 -> no verbosity (except on errors) [DEFAULT]
   * verbose = 1 -> display time profiling
   * verbose = 2 -> display WAP decomposition
   *)
  val compute :
    ?verbose:int ->
    ?export_CS:(string option) ->
    ?export_graphviz:(string option) ->
    ?wap_solver:wap_solver ->
    ?support_consistency:bool ->
      MLBDD.man ->
     ?ninput:(int option) ->
      int list -> MLBDD.t list -> MLBDD.t
end

module Argmax_UInt :
sig
  type wap_solver = Wap_exchange.input -> Wap_exchange.output

  (* [wap_of_system man param funlist = wap_input] where:
   *  - [man : MLBDD.man], is the MLBDD manager of the function in [funlist]
   *  - [param : int list], is the set of mode variables sorted in increasing order
   *  - [funlist : MLBDD.t list], is the set of Boolean Function which are to be conjuncted
   *  - [wap_input : Wap_exchange.input], is the WAP modelisation of this system
   *)
  val wap_of_system : MLBDD.man -> int list -> MlbddUInt.puint list -> Wap_exchange.input

  (* let default_wap_solver : wap_solver = Wap_greedy.lightweight_greedy *)
  val default_wap_solver : wap_solver

  (* [compute ?verbose:intverbose=0 ~wap_solver=default_wap_solver man param funlist = solved] where:
   *  - [man : MLBDD.man], is the MLBDD manager of the function in [funlist]
   *  - [param : int list], is the set of mode variables sorted in increasing order
   *  - [funlist : MLBDD.t list], is the set of Boolean Function which are to be conjuncted
   *  - [solved : MLBDD.t], is the conjunction of the Boolean Functions in [funlist]
   *
   * === NOTE ===
   * verbose = 0 -> no verbosity (except on errors) [DEFAULT]
   * verbose = 1 -> display time profiling
   * verbose = 2 -> display WAP decomposition
   *)
  val compute :
    ?verbose:int ->
    ?export_CS:(string option) ->
    ?export_graphviz:(string option) ->
    ?wap_solver:wap_solver ->
    ?halt_bpp:(MLBDD.man -> MLBDD.t list -> 'res option) ->
    ?support_consistency:bool ->
      MLBDD.man ->
     ?ninput:(int option) ->
      int list ->
      MlbddUInt.puint list -> (MLBDD.t, 'res option) result
end

module Argmax_UInt_Select :
sig
  type wap_solver = Wap_exchange.input -> Wap_exchange.output

  (* [wap_of_system man param funlist = wap_input] where:
   *  - [man : MLBDD.man], is the MLBDD manager of the function in [funlist]
   *  - [param : int list], is the set of mode variables sorted in increasing order
   *  - [funlist : MLBDD.t list], is the set of Boolean Function which are to be conjuncted
   *  - [wap_input : Wap_exchange.input], is the WAP modelisation of this system
   *)
  val wap_of_system : MLBDD.man -> int list -> MlbddUInt.puint list -> Wap_exchange.input

  (* let default_wap_solver : wap_solver = Wap_greedy.lightweight_greedy *)
  val default_wap_solver : wap_solver

  (* [compute ?verbose:intverbose=0 ~wap_solver=default_wap_solver man param funlist = solved] where:
   *  - [man : MLBDD.man], is the MLBDD manager of the function in [funlist]
   *  - [param : int list], is the set of mode variables sorted in increasing order
   *  - [funlist : MLBDD.t list], is the set of Boolean Function which are to be conjuncted
   *  - [solved : MLBDD.t], is the conjunction of the Boolean Functions in [funlist]
   *
   * === NOTE ===
   * verbose = 0 -> no verbosity (except on errors) [DEFAULT]
   * verbose = 1 -> display time profiling
   * verbose = 2 -> display WAP decomposition
   *)
  val compute :
    ?verbose:int ->
    ?export_CS:(string option) ->
    ?export_graphviz:(string option) ->
    ?wap_solver:wap_solver ->
    ?halt_bpp:(MLBDD.man -> MLBDD.t list -> 'res option) ->
    (* [halt_bpp man fl] takes two parameters:
     *   - [man] An MLBDD manager (the one passed to [compute] when calling the function).
     *   - [fl] A conjunction of Boolean constraints (represented as a list).
     * [halt_bpp] is called at the end of forward propagation by [compute].
     * If it returns [None], [compute] continues with the backward propagation.  
     * If it returns Some 'res ('res being an arbitrary type),
     *   the computation is interrupted prematurely and [compute] returns Error(Some res).
    *)
    ?support_consistency:bool ->
    (* default [ ~support_consistency:false ]
     * if [ ~support_consistency:true ], then [compute] checks that the support of constraints returned
     * after each projection step is consistent with CoSTreD hypothesis
     *)
     MLBDD.man ->
    ?ninput:(int option) ->
    (* default [ ~ninput:None ], then, [compute] infers [ ~ninput ] as the maximum index across all variables in the formula (plus 1)
     * [ ~ninput:(Some n) ] allows to specify the number of variables of the expression.
     * It is generally recommended to provide this information, as it avoids extra computation.
     * Note that the only anbiguity regarding the number of variables lies in useless variables, hence, in the number of solutions
     * The generated output is invariant by this number
     *)
     int list ->
     MlbddUInt.puint list -> ((MLBDD.var * MLBDD.t)list, 'res option) result
end

(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * Snowflake : A Generic Symbolic Dynamic Programming framework interfacing WAP, CoSTreD and MLBDD.
 *
 * Wap_lightspeed :
 *   Similar heuristic to lightweight but uses lazy H-reduction to reduce the complexitty to pseudo-linear
*)

open GuaCaml
open BTools
open GGLA_HFT.Type

val lightspeed_lopt : ?verbose:bool -> hg -> BNat.nat * (int list list)
val lightspeed_greedy : ?verbose:bool -> ?check:bool -> Wap_exchange.input -> Wap_exchange.output

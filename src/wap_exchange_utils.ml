(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * Snowflake : A Generic Symbolic Dynamic Programming framework interfacing WAP, CoSTreD and MLBDD.
 *
 * Wap_exchange_utils : toolbox for interfacing wap-solver with wap-exchange interface.
*)

open GuaCaml
open Wap_exchange

let prefix = "Snowflake.Wap_exchange_utils"

module ToS =
struct
  open STools
  open ToS

  let input (input:input) : string =
    SUtils.record [
      ("variables", list int input.variables);
      ("parameters", list int input.parameters);
      ("kdecomp", list(list int) input.kdecomp);
    ]

  let output_component (oc:output_component) : string =
    SUtils.record [
      ("index", int oc.index);
      ("is_param", bool oc.is_param);
      ("support", list int oc.support);
      ("internal", list int oc.internal);
      ("interface", list int oc.interface);
      ("var_interface", list int oc.var_interface);
      ("param_interface", list int oc.param_interface);
      ("tree_pred", option int oc.tree_pred);
    ]

  let output (output:output) : string =
    SUtils.record [
      ("input", input output.input);
      ("sequence", array output_component output.sequence);
      ("cost", list int output.cost);
      ("optimal", bool output.optimal);
    ]
end

module ToShiftS =
struct
  open STools
  open ShiftS
  open ToShiftS

  let input (input:input) : ShiftS.t =
    record [
      ("variables", list int input.variables);
      ("parameters", list int input.parameters);
      ("kdecomp", list(list int) input.kdecomp);
    ]

  let output_component (oc:output_component) : ShiftS.t =
    record [
      ("index", int oc.index);
      ("is_param", bool oc.is_param);
      ("support", list int oc.support);
      ("internal", list int oc.internal);
      ("interface", list int oc.interface);
      ("var_interface", list int oc.var_interface);
      ("param_interface", list int oc.param_interface);
      ("tree_pred", option int oc.tree_pred);
    ]

  let output (output:output) : ShiftS.t =
    record [
      ("input", input output.input);
      ("sequence", array output_component output.sequence);
      ("cost", list int output.cost);
      ("optimal", bool output.optimal);
    ]
end

module ToSTree =
struct
  open STools
  open ToSTree
  let input (input:input) : Tree.stree =
    trio  (list int)       (list int)        (list(list int))
         (input.variables, input.parameters, input.kdecomp  )

  let output_component (oc:output_component) : Tree.stree =
    Tree.Node [
      int oc.index;
      bool oc.is_param;
      list int oc.support;
      list int oc.internal;
      list int oc.interface;
      list int oc.var_interface;
      list int oc.param_interface;
      option int oc.tree_pred
    ]

  let output (output:output) : Tree.stree =
    Tree.Node [
      input output.input;
      array output_component output.sequence;
      list int output.cost;
      bool output.optimal;
    ]
end

module OfStree =
struct
  open STools
  open OfSTree

  let input (t:Tree.stree) : input =
    let variables, parameters, kdecomp =
      trio (list int) (list int) (list(list int)) t
    in
    {variables; parameters; kdecomp}

  let output_component (t:Tree.stree) : output_component  =
    match t with
    | Tree.Node [
      index;
      is_param;
      support;
      internal;
      interface;
      var_interface;
      param_interface;
      tree_pred;
    ] -> {
      index = int index;
      is_param = bool is_param;
      support = list int support;
      internal = list int internal;
      interface = list int interface;
      var_interface = list int var_interface;
      param_interface = list int param_interface;
      tree_pred = option int tree_pred
    }
    | _ -> assert false

  let output (t:Tree.stree) : output =
    match t with
    | Tree.Node [
      t_input;
      sequence;
      cost;
      optimal;
    ] -> {
      input = input t_input;
      sequence = array output_component sequence;
      cost = list int cost;
      optimal = bool optimal;
    }
    | _ -> assert false
end

let check_input (graph:Wap_exchange.input) : bool =
  (SetList.sorted_nat graph.variables) &&
  (SetList.sorted_nat graph.parameters) &&
  (SetList.nointer graph.variables graph.parameters) &&
  (List.for_all SetList.sorted_nat graph.kdecomp) &&
  (
    let nodes = SetList.union graph.variables graph.parameters in
    List.for_all (fun k -> SetList.subset_of k nodes) graph.kdecomp
  )

let check_output_component (i:int) (len:int) (comp:output_component) : bool =
  (comp.index = i) &&
  match comp.tree_pred with
  | Some pred -> (
    (comp.is_param = false) &&
    (pred > comp.index) &&
    (SetList.subset_of comp.internal comp.support) &&
    (comp.interface = SetList.minus comp.support comp.internal) &&
    (SetList.subset_of comp.var_interface comp.interface) &&
    (SetList.subset_of comp.param_interface comp.interface) &&
    (* the three following are meant to be redundant *)
    (SetList.nointer comp.var_interface comp.param_interface) &&
    (comp.param_interface = SetList.minus comp.interface comp.var_interface) &&
    (comp.var_interface = SetList.minus comp.interface comp.param_interface)
  )
  | None -> (
    (comp.is_param = true ) &&
    (comp.internal = []) &&
    (comp.interface = []) &&
    (comp.var_interface = []) &&
    (comp.param_interface = comp.support)
  )

let rec check_sequence_pred ?(i=0) (seq:sequence) : bool =
  let n = Array.length seq in
  if i < n
  then (
    let seq_i = seq.(i) in
    (check_output_component i n seq_i) &&
    (
      match seq_i.tree_pred with
      | Some pred -> (
        let seq_pred = seq.(pred) in
        (SetList.subset_of seq_i.interface seq_pred.support) &&
        (seq_i.interface = SetList.inter seq_i.support seq_pred.support)
      )
      | None      -> true
    ) &&
    (check_sequence_pred ~i:(succ i) seq)
  )
  else true

let check_output (output:Wap_exchange.output) : bool =
  (check_input output.input) &&
  (check_sequence_pred output.sequence)

let assert_input (graph:Wap_exchange.input) : unit =
       if not (SetList.sorted_nat graph.variables)
    then failwith "[Snowflake.wap_exchange_utils.assert_input] graph.variables is not sorted"
  else if not (SetList.sorted_nat graph.parameters)
    then failwith "[Snowflake.wap_exchange_utils.assert_input] graph.parameters is not sorted"
  else if not (SetList.nointer graph.variables graph.parameters)
    then failwith "[Snowflake.wap_exchange_utils.assert_input] graph.variables and graph.parameters have common variables"
  else if not (List.for_all SetList.sorted_nat graph.kdecomp)
    then failwith "[Snowflake.wap_exchange_utils.assert_input] graph.kdecomp elements are not sorted"
  else (
    let nodes = SetList.union graph.variables graph.parameters in
    if not (List.for_all (fun k -> SetList.subset_of k nodes) graph.kdecomp)
    then failwith "[Snowflake.wap_exchange_utils.assert_input] graph.kdecomp elements are not included in available nodes"
    else ()
  )

let rec assert_sequence_pred ?(i=0) (seq:sequence) : unit =
  let n = Array.length seq in
  if i < n
  then (
    let seq_i = seq.(i) in
         if not (check_output_component i n seq_i)
    then failwith ("[Snowflake.wap_exchange_utils.assert_sequence_pred] seq.("^(STools.ToS.int i)^") is not output_component's checked")
    else (
      (
        match seq_i.tree_pred with
        | Some pred -> (
          let seq_pred = seq.(pred) in
               if not (SetList.subset_of seq_i.interface seq_pred.support)
            then failwith ("[Snowflake.wap_exchange_utils.assert_sequence_pred] seq.("^(STools.ToS.int i)^")'s interface is not includes in its predecessors's support")
          else if not (seq_i.interface = SetList.inter seq_i.support seq_pred.support)
            then failwith ("[Snowflake.wap_exchange_utils.assert_sequence_pred] seq.("^(STools.ToS.int i)^")'s interface is not equal to seq.(i).support \\inter seq.(pred).support")
            else ()
        )
        | None      -> ()
      );
      (assert_sequence_pred ~i:(succ i) seq)
    )
  )

let assert_output (output:Wap_exchange.output) : unit =
  (assert_input output.input);
  (assert_sequence_pred output.sequence)

let list_sort l = List.sort Stdlib.compare l

(* sort lists *)
let normalize_input (input:input) : input =
  {
    variables = list_sort input.variables;
    parameters = list_sort input.parameters;
    kdecomp = List.map list_sort input.kdecomp;
  }

(* operator_SO returns an output_component
  where {neighborhood, internal, interface, var_interface, param_interface} have been initialized
  and {index, tree_pred} have not been initialized
 *)

let operator_SO (input:input) (vl:int list) : output_component * input =
  (* print_string STools.ToS.("[operator_SO] vl:"^(list int vl)); print_newline(); *)
  let internal = list_sort vl in
  (* print_string STools.ToS.("[operator_SO] internal:"^(list int internal)); print_newline(); *)
  (* print_string STools.ToS.("[operator_SO] input.variables:"^(list int input.variables)); print_newline(); *)
  assert(SetList.subset_of internal input.variables);
  (* ng = neighborhood
     nng = no neighborhood *)
  let ng, nng = MyList.partition (SetList.nointer internal) input.kdecomp in
  (* flat neighborhood *)
  let support = SetList.union (List.fold_left SetList.union [] ng) internal in
  (* strict neighborhood *)
  let interface = SetList.minus support internal in
  let input' = {
    variables  = SetList.minus input.variables internal;
    parameters = input.parameters;
    kdecomp = if interface = []
      then               nng
      else (interface :: nng)
  } in
  let output = {
    index = 0;
    is_param = false;
    support;
    internal;
    interface;
    var_interface = SetList.inter interface input.variables;
    param_interface = SetList.inter interface input.parameters;
    tree_pred = None
  } in
  (output, input')

(* [update_tree] allows to properly setup the [tree_pred] field for each component
   Time Complexity O(nk) where
    - n = 'number of vertices' and
    - k = 'suppressing sequence size'
 *)
let update_tree ?(vlen=None) (output:output) : unit =
  (* initialize [vlen] *)
  let vlen0 = vlen in
  let vlen =
    match vlen with
    | Some vlen -> vlen
    | None -> (
      let inputs = SetList.union
        output.input.variables
        output.input.parameters in
      assert(SetList.sorted_nat inputs);
      match MyList.opget_last inputs with
      | Some maxi -> succ maxi
      | None      -> 0
    )
  in
  let seq = output.sequence in
  let len = Array.length seq in
  let last = Array.make vlen (-1) in
  let last_param = ref (len-1) in
  assert(len = 0 || seq.(len-1).is_param);
  for i = len - 1 downto 0
  do
    assert(seq.(i).index = i);
    if seq.(i).is_param
    (* seq.(i) is a parametere type component *)
    then (
      last_param := i;
      List.iter (fun j -> last.(j) <- i) seq.(i).support;
    )
    (* seq.(i) is a variable type component *)
    else (
      let pred = ref !last_param in
      (* compute minimum ancestor *)
      List.iter
        (fun j -> pred := min !pred last.(j))
        seq.(i).interface;
      assert(seq.(i).interface = SetList.minus seq.(i).support seq.(i).internal);
      let pred = !pred in
      if not (i < pred && pred < len)
      then (
        print_endline "[Snowflake.wap_exchange_utils.update_tree ~vlen output] error:'i < pred && pred < len'";
        print_endline ("vlen:"^(STools.ToS.(option int) vlen0));
        print_endline ("output:"^(ToS.output output));
        print_endline ("i:"^(STools.ToS.int i));
        print_endline ("pred:"^(STools.ToS.int pred));
        print_endline ("len:"^(STools.ToS.int len));
        failwith "[Snowflake.wap_exchange_utils.update_tree] inconsistent tree : contact development team"
      );
      (* update [seq.(i)]'s ancestor *)
      seq.(i) <- {seq.(i) with tree_pred = Some pred};
      (* override minimum ancestor *)
      List.iter (fun j -> last.(j) <- i) seq.(i).internal;
    );
  done

(* [operator_SSO input vll] takes :
    - an [input] problem description and
    - a supressing sequence [vll]
    and returns
    - an annotated suppressing sequence [output]
 *)
let operator_SSO ?(verbose=false) ?(optimal=false) (input:input) (vll:int list list) : output =
  let prefix = "["^prefix^".operator_SS0]" in
  let k, outl, cost, inp = List.fold_left
    (fun (i, out, cost, inp) vl ->
      let out', inp' = operator_SO inp vl in
      let out' = {out' with index = i} in
      let cost' = List.length out'.support in
      (succ i, out'::out, cost'::cost, inp')
    )
    (0, [], [], input) vll
  in
  if List.length outl <> k
  then (invalid_arg (prefix^" internal inconsistency, please contact support"));
  if inp.variables <> []
  then (print_endline (prefix^" [warning] some variable do not appear in the given sequence"));
  if inp.parameters <> input.parameters
  then (print_endline (prefix^" [warning] some parameters are not preserved"));
  (* create an extra output_component for parameter variables *)
  let param_comp = {
    index = k;
    is_param = true;
    support  = input.parameters;
    internal = [];
    interface = [];
    var_interface = [];
    param_interface = input.parameters;
    tree_pred = None
  } in
  if verbose then print_endline ("[operator_SSO] cost(not normalized):"^(STools.ToS.(list int)(List.rev cost)));
  let output = {
    input;
    sequence = Array.of_list (List.rev (param_comp::outl));
    cost = Bicimal.normalize (List.rev cost);
    optimal;
  } in
  (* print_endline ("[Snowflake.wap_exchange_utils.operator_SSO] output:"^(ToS.output output)); *)
  update_tree output;
  (* print_endline ("[Snowflake.wap_exchange_utils.operator_SSO] output:"^(ToS.output output)); *)
  assert_output output;
  output
  |> Tools.check check_output

let to_GraphKD ?(verbose=0) ?(check=true) (graph:Wap_exchange.input) : GraphKD.graph =
  let profile (text:string) =
    if verbose > 0
    then OProfile.(profile default) ("[Snowflake.Wap_exchange_utils.to_GGLA_HFT] "^text)
    else (fun f -> f)
  in
  let profile2 (text:string) =
    if verbose > 0
    then OProfile.(profile2 default) ("[Snowflake.Wap_exchange_utils.to_GGLA_HFT] "^text)
    else (fun f -> f)
  in
  if check then profile "assert_input" assert_input graph;
  (* convert to [GraphKD] *)
  let nodes = profile2 "compute nodes" SetList.union graph.variables graph.parameters in
  GraphKD.{nodes; fixed = graph.parameters; cliques = graph.kdecomp}
  |> Tools.check ~debug_only:check (profile "GraphKD.check" GraphKD.check)

type graph_output_model =
  | GOM_FT
  | GOM_HFT
  | GOM_CFT

let to_graphviz_file ?(verbose=0) ?(check=true) ?(gom=GOM_HFT) ?(astree=true) ?(name="") (file:string) (graph:Wap_exchange.input) : unit =
  let profile (text:string) =
    if verbose > 0
    then OProfile.(profile default) ("[Snowflake.Wap_exchange_utils.to_graphviz_file] "^text)
    else (fun f -> f)
  in
  let kd =
    profile "to_GraphKD"
      (to_GraphKD ~verbose:(pred verbose) ~check)
       graph
  in
  match gom with
  | GOM_FT -> (
    kd
    |> profile "GGLA_FT.of_GraphKD" GGLA_FT.of_GraphKD
    |> profile "GGLA_FT.to_graphviz_file"
        (GGLA_FT.to_graphviz_file ~astree ~name file)
  )
  | GOM_HFT -> (
    kd
    |> profile "GGLA_HFT.of_GraphKD"
        (GGLA_HFT.of_GraphKD ~remove_useless:true)
    |> profile "GGLA_HFT.to_graphviz_file"
        (GGLA_HFT.to_graphviz_file ~astree ~name file)
  )
  | GOM_CFT -> (
    kd
    |> profile "GGLA_CFT.of_GraphKD"
        (GGLA_CFT.of_GraphKD ~remove_useless:true)
    |> profile "GGLA_CFT.to_graphviz_file"
        (GGLA_CFT.to_graphviz_file ~astree ~name file)
  )

let ocaml_file_of_boolean_constraint_system
   ?(ninput=None)
    (variables:int list)
    (parameters:int list)
    (fl:MLBDD.t list)
    (target:string) : unit =
  assert(SetList.sorted variables);
  assert(SetList.sorted parameters);
  let open STools in
  let open STools.ToS in
  let open MlbddUtils.ToS in
  let text = SUtils.record
    [
      ("variables", list int variables);
      ("parameters", list int parameters);
      ("constraints", list (fun x_ -> "\n\t"^(f x_)) fl);
    ]
  in
  STools.SUtils.output_string_to_file target text

let ocaml_file_of_puint_constraint_system
   ?(ninput=None)
    (variables:int list)
    (parameters:int list)
    (fl:MlbddUInt.puint list)
    (target:string) : unit =
  assert(SetList.sorted variables);
  assert(SetList.sorted parameters);
  let open STools in
  let open STools.ToS in
  let open MlbddUInt.ToS in
  let text = SUtils.record
    [
      ("variables", list int variables);
      ("parameters", list int parameters);
      ("constraints", list (fun x_ -> "\n\t"^(puint x_)) fl);
    ]
  in
  STools.SUtils.output_string_to_file target text

let cir_file_of_boolean_constraint_system
   ?(ninput=None)
    (variables:int list)
    (parameters:int list)
    (fl:MLBDD.t list)
    (target:string) : unit =
  assert(SetList.sorted variables);
  assert(SetList.sorted parameters);
  let open Extra in
  let open STools in
  let open STools.ToS in
  let open MlbddUtils.ToCir in
  let line1 = "c file generated with Snowflake" in
  let ninput = match ninput with
    | Some ninput -> ninput
    | None -> (
      let variables = SetList.union variables parameters in
      print_endline (prefix^" [export_CS] [warning] number of expressed variables : "^(int(List.length variables)));
      let ninput = 1+(List.fold_left max (-1) variables) in
      print_endline (prefix^" [export_CS] [warning] guessing the number of variables to be "^(int ninput));
      ninput
    )
  in
  let line2 = ("p cir "^(int ninput)^" "^(int(List.length fl))) in
  let line3 = ("f "^(SUtils.catmap " " (fun i -> int(succ i)) parameters)) in
  fl
  ||> MlbddUtils.ToCir.f
  |> (fun body -> line1 :: line2 :: line3 :: body)
  |> String.concat "\n"
  |> STools.SUtils.output_string_to_file target

let cir_file_of_puint_constraint_system
   ?(ninput=None)
    (variables:int list)
    (parameters:int list)
    (fl:MlbddUInt.puint list)
    (target:string) : unit =
  assert(SetList.sorted variables);
  assert(SetList.sorted parameters);
  let open Extra in
  let open STools in
  let open STools.ToS in
  let open MlbddUInt.ToCir in
  let line1 = "c file generated with Snowflake" in
  let ninput = match ninput with
    | Some ninput -> ninput
    | None -> (
      let variables = SetList.union variables parameters in
      print_endline (prefix^" [export_CS] [warning] number of expressed variables : "^(int(List.length variables)));
      let ninput = 1+(List.fold_left max (-1) variables) in
      print_endline (prefix^" [export_CS] [warning] guessing the number of variables to be "^(int ninput));
      ninput
    )
  in
  let line2 = ("p cir "^(int ninput)^" "^(int(List.length fl))) in
  let line3 = ("f "^(SUtils.catmap " " (fun i -> int(succ i)) parameters)) in
  fl
  ||> MlbddUInt.ToCir.puint
  |> MyList.flatten
  |> (fun body -> line1 :: line2 :: line3 :: body)
  |> String.concat "\n"
  |> STools.SUtils.output_string_to_file target

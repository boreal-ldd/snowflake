(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * Snowflake : A Generic Symbolic Dynamic Programming framework interfacing WAP, CoSTreD and
 *
 * MlbddUtils : MLBDD toolbox
 *)

open GuaCaml
open STools

module OOPS :
sig
  type t = MLBDD.man
  type f = {arity : int; bdd : MLBDD.t}

  (*  val ( ->> ) : t -> bool list -> f -> f *)
  val arity   : t -> f -> int
  val   cneg : t -> bool -> f -> f
  val ( &! ) : t -> f -> f -> f
  val ( |! ) : t -> f -> f -> f
  val ( ^! ) : t -> f -> f -> f
  val ( =! ) : t -> f -> f -> f
  val ( *! ) : t -> f -> f -> f

  val cst : t -> bool -> int -> f
  val var : t -> bool -> int -> int -> f
  (* [var t b0 n k = f]
      where [f] is the function (b:\B^n) -> b0 \xor b_k
   *)

  (* return a consistent projection of fs
     to [Some bool] or [None] otherwise *)
  val to_bool : t -> f -> bool option
end

type t = MLBDD.man
type f = MLBDD.t

type supp = MLBDD.var list (* MLBDD.var = int *)

val cst : t -> bool -> f

val ( ~! ) : f -> f
val cneg : bool -> f -> f
val ( &! ) : f -> f -> f
val ( |! ) : f -> f -> f
val ( ^! ) : f -> f -> f
val ( =! ) : f -> f -> f

val var : t -> bool -> MLBDD.var -> f

(* [sorted_support f = il] where :
 * - [il] is the indexes of the variables in the support of [f]
 * - [il] is sorted in increasing order (compatible with GuaCaml.SetList)
 *)
val sorted_support : f -> supp

(*
 * [is_trivial f] returns
 * - [Some true ] if [f] is the constant true
 * - [Some false] if [f] is the constant false
 * - [None] if [f] is not a constant
 *)
val is_trivial : f -> bool option

(*
 * [lexand t fl = f]
 * where [f] is the BDD obtained by conjuncting the functions in the list [fl]
 * [lexand] firsts sorts the term according to the lexicographic order on their support,
 *   then conjunct them backward
 *)
val lexand : t -> f list -> f

(* val ite_list : f list -> MLBDD.var -> f list -> f list *)

val ite_list : f list -> MLBDD.var -> f list -> f list

(* Evaluation of a BDD from the sorted association list (assoc:(MLBDD.var -> bool)) where:
 *   - dom(assoc) supersets support(phi)
 *)

val eval : f -> (MLBDD.var * bool)list -> bool

(* Partial Evaluation of a BDD using the sorted association list (assoc:(MLBDD.var -> bool)) where:
 *)
(* partial eval from ordered lists of both BDD variables and their valuation,
 * using the memoization procedure by Joan Thibault *)

val peval : f -> (MLBDD.var * bool)list -> f

(* similar to [ite f0 fc f1] excepts that fc is a function and not a variable
 *)

val fite : f -> f -> f -> f

(* variable substitution by functions (using an association list from variables [var] to functions [t])
 * [subst phi [(x1, psi1); (x2, psi2); ...] = phi[x1:=psi1, x2:=psi2, ...]
 * the settic of this operation is easily defined iff supp(phi_i) \inter (x1, ..., xk) is empty
 * otherwise one must understand that all substitutions are performed on the input formula
 * e.g. [subst  {x1 /\ x2} [(x1, {x2}); (x2, {\lnot x1}) = {x2 /\ \lnot x1}] ( = {(\lnot x1) /\ x2} )
 * In practice, this result is obtained by completely exploring the formula before to rebuilt it in one go.
 *)

val subst : f -> (MLBDD.var * f)list -> f

(* val sat_select : f -> supp -> f * f
 * [sat_select f suppB = g * f'] where :
 * - [f  : bool^(suppA) -> bool^(suppB) -> bool] (where suppA = support(f) - suppB)
 * - [f' : bool^(suppA) -> bool^(suppB) -> bool]
 * - [g  : bool^(suppA)                 -> bool]
 * such that :
 * - [suppB is SetList.sorted]
 * - [f' => f]
 * - [\forall (a:suppA). (\exists! (b:suppB) f'(a, b)) \/ f(a, .) = false_{suppB}]
 * - [\exists(suppB) f' = \exists(suppB) f]
 *   + i.e. [\forall (a:suppA). (\exists (b:suppB) f'(a, b)) <=> (\exists (b:suppB) f(a, b))]
 * - [\forall (a:suppA). (\forall (b1 b2:suppB) f'(a, b1) = true -> f'(a, b2) = true -> b1 = b2)]
 * - [g = \exists(suppB) f]
 * === In Other Words ===
 * We define [suppA := support(f) - suppB] the set of parameter variables.
 * For any valuation of parameters variables (a:suppA), we denote by
 * [f(a, .) := peval f (List.combine suppA a)] the subfunction of f[suppA:=a].
 * Either :
 * (1) [is_false f(a, .) = true ] (i.e., [f(a. .)] is unsatisfiable, i.e., [f(a, .)] has no solution)
 *   Then, [g(a) = False] and [f'(a, .) = False], or,
 * (2) [is_false f(a, .) = false] (i.e., [f(a, .)] is satisfiable, i.e. [f(a, .)] has at least one solution)
 *   Then, [g(a) = True ] and [f'(a, .)] has exacly one solution (b:suppB) such that [f(a, b) = True]
 *   (i.e. [b] is a solution of [f(a, .)])
 *)

val sat_select : f -> supp -> f * f

(* val exist_proj : f -> supp -> f * (MLBDD.var * f) list
 * [exists_proj f suppB = g * (assoc[(v, p_v)])] where :
 * - [f  : bool^(suppA) -> bool^(suppB) -> bool] (where suppA = support(f) - suppB)
 * - [g  : bool^(suppA)                 -> bool]
 * such that :
 * - [suppB is SetList.sorted]
 * - [g = \exists(suppB) f]
 * - [assoc is Assoc.sorted]
 * - [dom(assoc) = suppB]
 * - [\forall v\in suppB, p_v : bool^(suppA) -> bool]
 *   + i.e., [[assoc] : suppB -> bool^(suppA) -> bool)]
 * - [\forall v\in suppB, p_v := \exists(suppB - {v}) phi)[v:=1]]
 * === In Other Words ===
 * [exists_proj phi suppB = (guard, assoc)] takes as input a BDD
 * [phi : bool^suppA -> bool^suppB -> bool] (where suppA := support(f) - suppB)
 * and a sorted list of variables [suppB].
 * It returns [g = \exists(suppB) phi : bool^(suppA) -> bool] the set of modes
 * for which there exists a solution and [assoc:suppB -> (bool^(suppA) -> bool)]
 * a sorted association list which associate to each variable [v] in [suppB] a
 * BDD [p_v := (\exists(suppB - {v}) phi)[v:=1] : bool^(suppA) -> bool].
 *)

val exists_proj : f -> supp -> f * (MLBDD.var * f) list

val argmax : f -> MLBDD.support -> f -> f * f

val lexmax : f -> MLBDD.support -> f array -> f * (f array)
val lexmax_rev : f -> MLBDD.support -> f array -> f * (f array)

type psubst = f * (MLBDD.var * f) list

module ToS :
sig
  open STools.ToS

  val f : ?man:string -> f t
  val psubst : ?man:string -> psubst t
end

module ToCir :
sig
  val f : f -> string
  val psubst : psubst -> string
end

module PSubst :
sig
  val sorted_domain : psubst -> supp
  val domain : psubst -> MLBDD.support
  val sorted_codomain : psubst -> supp
  val codomain : psubst -> MLBDD.support
  val sorted_support : psubst -> supp
  val support : psubst -> MLBDD.support
  val subst : f -> psubst -> f
  val of_guard : f -> supp -> psubst
  val is_trivial : psubst -> bool option
  val equal : psubst -> psubst -> bool
end

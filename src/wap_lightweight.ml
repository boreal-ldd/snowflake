(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * Snowflake : A Generic Symbolic Dynamic Programming framework interfacing WAP, CoSTreD and MLBDD.
 *
 * Wap_lightweight :
 *   Simple heuristic for WAP, which iteratively select a vertex with lightest neighborhood then
 *   apply H-reduction (true-twin merging).
*)

open GuaCaml
open Extra
open STools
open BTools
open GGLA_HFT.Type
open GraphGenLA.Type

let prefix = "Snowflake.Wap_lightweight"

let opmin
  (opx:(BNat.nat * 'a)option)
  ((y, vy):(BNat.nat*'a)) : (BNat.nat * 'a) option =
  match opx with
  | None -> Some(y, vy)
  | Some(x, vx) ->
    if BNat.(</) y x
    then Some(y, vy)
    else opx

let find_best graph =
  Array.fold_left (fun opbest v ->
    (* filter with [v.tag.vtag_alive = true] *)
    if not (GGLA_HFT.vertex_fixed v)
    then (
      let local_cost : BNat.nat =
        GGLA_HFT.wap_Sc graph v.index
      in
      opmin opbest (local_cost, v.index)
    )
    else opbest
  ) None graph

let lightweight_find_lopt graph =
  let rec aux carry graph total_cost =
    match find_best graph with
    | None -> (
      assert(Array.for_all GGLA_HFT.vertex_fixed graph);
        (* i.e., \forall i graph.(i).tag.vtag_fixed = true *)
      (total_cost, List.rev carry)
    )
    | Some (best_cost, best) -> (
      let names = GGLA_HFT.vertex_names graph.(best) in
      let cost, next_graph = GGLA_HFT.wap_S graph best in
      assert BNat.(cost =/ best_cost);
      (* print_endline ("[lightweight_find_lop] cost:"^(BNat.to_string cost));
         print_endline ("[lightweight_find_lop] cost:"^(cost |> BNat.to_bicimal |> ToS.(list int))); *)
      aux (names::carry) next_graph BNat.(total_cost +/ cost)
    )
  in aux [] graph (BNat.zero())

let lightweight_greedy ?(verbose=false) ?(check=false) (input:Wap_exchange.input) : Wap_exchange.output =
  let prefix = "["^prefix^".lightweight_greedy]" in
  let stop = OProfile.(time_start default) (prefix^" to_GGLA_HFT") in
  let kd : GraphKD.graph = Wap_exchange_utils.to_GraphKD ~check input in
  let hg : GGLA_HFT.Type.hg = GGLA_HFT.of_GraphKD kd in
  stop();
  let stop = OProfile.(time_start default) (prefix^" find_lopt") in
  let bnat_cost, seq = lightweight_find_lopt hg in
  stop();
  if verbose then print_endline ("[lightweight_greedy] seq:"^(STools.ToS.(list(list int)) seq));
  let stop = OProfile.(time_start default) (prefix^" operator_SSO") in
  let output = Wap_exchange_utils.operator_SSO ~verbose input seq in
  stop();
  (* Check Cost Signature *)
  let bici_cost = output.Wap_exchange.cost in
  let bici_bnat_cost = BNat.to_bicimal bnat_cost in
  (if not (bici_bnat_cost = bici_cost)
  then (
    print_endline ("[lightweight_greedy] bici_cost     :"^(STools.ToS.(list int) bici_cost));
    print_endline ("[lightweight_greedy] bici_bnat_cost:"^(STools.ToS.(list int) bici_bnat_cost));
    print_endline ("[lightweight_greedy] bnat_cost:"^(BNat.to_string bnat_cost));
    assert false
  ));
  output

